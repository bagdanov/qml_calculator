# QML Calculator

Several students asked about linking python-defined models with
QML-defined interfaces. Here is a quick-and-dirty skeleton for how to
do this. The calculator only implements digit insertion and input
clear.

Some highlights:

* `calculatormodel.py`: the definition of CalculatorModel, which will
  hold the properties, slots, and signals needed to provide
  functionality to the UI.
* `qml_calculator.py`: the main application, check out how the model
  is instantiated and then **registered** with the QML Engine.
* `qml_calculator.qml`: the definition of the GUI, only the essential
  bits. Note how I differentiate between `NumberButton` and
  `OperationButton`.
* `NumberButton.qml`: the QML definition of a button that
  **self-inserts** its text property using a method in the Model.


