import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: window
    visible: true
    title: "QML Calculator"
    width: mainlayout.width   // Connect window size with content size.
    height: mainlayout.height
    ColumnLayout {
	id: mainlayout
	GridLayout {
	    columns: 4
	    anchors.fill: parent
	    Label {
		Layout.columnSpan: 4
		Layout.fillWidth: true; Layout.fillHeight: true;
		horizontalAlignment: Text.AlignRight
		font.pointSize: 20
		text: Model.inputText   // This is a dynamic property from the *Model*.
	    }
	    NumberButton   { text: "7" } // Row 1.
	    NumberButton   { text: "8" }
	    NumberButton   { text: "9" }
	    OperatorButton { text: "+" }
	    NumberButton   { text: "4" } // Row 2.
	    NumberButton   { text: "5" }
	    NumberButton   { text: "6" }
	    OperatorButton { text: "-" }
	    NumberButton   { text: "1" } // Row 3.
	    NumberButton   { text: "2" }
	    NumberButton   { text: "3" }
	    OperatorButton { text: "*" }
	    OperatorButton { text: "C"; onClicked: { Model.clear(); } } // Row 4
	    NumberButton   { text: "0" }
	    OperatorButton { text: "=" }
	    OperatorButton { text: "/" }
	}
    }
}

