from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, pyqtProperty

# The beginning of a model for our calculators.
class CalculatorModel(QObject):
    inputTextChanged = pyqtSignal(str)
    
    def __init__(self):
        super().__init__(parent=None)
        self._inputText = '0'
        self._inserting = False
        self._accumulator = 0

    # A text property that holds the current input text.
    @pyqtProperty(str, notify=inputTextChanged)
    def inputText(self):
        return self._inputText

    @inputText.setter
    def inputText(self, newval):
        self._inputText = newval
        self.inputTextChanged.emit(newval)

    # Method to handle digit insertion. Tracks input status.
    @pyqtSlot(str)
    def insert_digit(self, d):
        if self._inserting:
            self.inputText += d
        else:
            self.inputText = d
            self._inserting = True

    # Clear input.
    @pyqtSlot()
    def clear(self):
        self._inserting = False
        self.inputText = '0'
