from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QUrl
from calculatormodel import CalculatorModel
from OpenGL import GL

# Create a QApplication.
app = QApplication([])

# Instantiate the QML Engine.
engine = QQmlApplicationEngine(QUrl('./qml_calculator.qml'))

# Create the calculator model, and set it as a context property.
model = CalculatorModel()
engine.rootContext().setContextProperty('Model', model)

# Let 'er rip.
app.exec_()
